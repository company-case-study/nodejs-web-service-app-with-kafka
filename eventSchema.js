const avro = require('avsc')

module.exports = avro.Type.forSchema({
    type: 'array',
    items: {
      type: 'record',
      fields: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'buyingPrice',
          type: [
            'string',
            'double',
          ],
        },
        {
          name: 'sellingPrice',
          type: [
            'string',
            'double',
          ],
        }
      ]
    }
  });
