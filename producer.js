const axios = require('axios');
const { XMLParser } = require('fast-xml-parser');
const { Kafka, Partitioners } = require('kafkajs')

const eventSchema = require('./eventSchema')

const kafka = new Kafka({
  clientId: 'producer-in-node',
  brokers: ['localhost:9092'],
})

const producer = kafka.producer({ createPartitioner: Partitioners.DefaultPartitioner })

producer.connect().then(() => setInterval(() => {
  axios
    .get('https://www.tcmb.gov.tr/kurlar/today.xml')
    .then(function (response) {
        const parser = new XMLParser({
            ignoreAttributes: false,
        });
        const data = parser.parse(response.data);
        const currencyList = data.Tarih_Date.Currency.map((currency) => ({
            name: `${currency['@_CurrencyCode']}TRY`,
            buyingPrice: currency.BanknoteBuying || currency.ForexBuying,
            sellingPrice: currency.BanknoteSelling || currency.ForexSelling,
        }));
        producer.send({
          topic: 'istegelsin',
          messages: [
            { value: eventSchema.toBuffer(currencyList) },
          ],
        })
        console.log('data sending.')
    })
    .catch(function (error) {
        console.error('upps. not connecting to TCMB. check your network');
        console.log(error);
    });
}, 10000))

