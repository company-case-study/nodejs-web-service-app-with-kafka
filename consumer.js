const { Kafka } = require('kafkajs');

const eventSchema = require('./eventSchema')

const kafka = new Kafka({
    clientId: 'consumer-in-node',
    brokers: ['localhost:9092'],
});
const consumer = kafka.consumer({ groupId: 'financial-department' });

consumer.subscribe({ topic: 'istegelsin', fromBeginning: true }).then(
    consumer.run({
        eachMessage: async ({ topic, partition, message }) => {
            console.log('data receving.')
            console.log({
                value: eventSchema.fromBuffer(message.value),
            });
        },
    })
);
