# Nodejs Web Service App With Kafka

## Step 1:
```
yarn install
```
Or
```
npm install
```

## Step 2:
```
yarn run start:kafka
```
Or
```
npm run start:kafka
```

## Step 3:
```
yarn run init:kafka
```
Or
```
npm run init:kafka
```

## Step 4:
```
yarn run start:producer
```
Or
```
npm run start:producer
```

## Step 5:
```
yarn run start:consumer
```
Or
```
npm run start:consumer
```